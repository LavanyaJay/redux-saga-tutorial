//Align saga structure with actions structure
import {
  takeEvery,
  call,
  fork,
  put,
  takeLatest,
  take,
} from 'redux-saga/effects';
import * as actions from '../actions/users';
import * as api from '../api/users';
//Watch saga- whenever get req action is dispatched
//call helps us to call a promise sequentially, waiting for it to resolve without callbacks
//calling fork within the main process calls the child process- root saga will be the main and all watcher sagas will be forked from the root saga
//put effect to dispatch an action
//takeLatest effect to create a new user
function* getUsers() {
  //workers saga
  try {
    const result = yield call(api.getUsers);
    yield put(
      actions.getUsersSuccess({
        items: result.data.data,
      })
    );
  } catch (e) {
    yield put(
      actions.usersError({
        error: 'An error occurred',
      })
    );
  }
}
function* watchGetUserRequest() {
  yield takeEvery(actions.Types.GET_USERS_REQUEST, getUsers);
}

function* createUser(action) {
  try {
    yield call(api.createUser, {
      firstName: action.payload.firstName,
      lastName: action.payload.lastName,
    });
    yield call(getUsers);
  } catch (e) {
    yield put(
      actions.usersError({
        error: 'An error occurred',
      })
    );
  }
}

function* watchCreateUserRequest() {
  yield takeLatest(actions.Types.CREATE_USER_REQUEST, createUser);
}
function* deleteUser(id) {
  try {
    yield call(api.deleteUser, id);
    yield call(getUsers);
  } catch (e) {
    yield put(
      actions.usersError({
        error: 'An error occurred',
      })
    );
  }
}
function* watchDeleteUserRequest() {
  while (true) {
    const action = yield take(actions.Types.DELETE_USER_REQUEST);
    yield call(deleteUser, action.payload.id);
  }
}
const usersSagas = [
  fork(watchGetUserRequest),
  fork(watchCreateUserRequest),
  fork(watchDeleteUserRequest),
];

export default usersSagas;
