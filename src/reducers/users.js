import { Types } from '../actions/users';
var _ = require('lodash');
export const users = (state = { items: [], error: '' }, action) => {
  switch (action.type) {
    case Types.GET_USERS_SUCCESS: {
      const sortedResult = _.orderBy(action.items, ['firstName', 'lastName']);
      return {
        ...state,
        items: sortedResult,
      };
    }
    case Types.USER_ERROR: {
      console.log(action.payload.error);
      return {
        ...state,
        error: action.payload.error,
      };
    }
    default:
      return state;
  }
};
