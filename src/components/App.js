import React from 'react';
import { connect } from 'react-redux';
import {
  getUsersRequest,
  deleteUserRequest,
  usersError,
} from '../actions/users';
import UsersList from '../components/UsersList';
import CreateUserForm from './CreateUserForm';
import { Alert } from 'reactstrap';
class App extends React.Component {
  constructor(props) {
    super(props);
    this.props.getUsersRequest();
  }
  handleCloseAlert = () => {
    this.props.usersError({ error: '' });
  };
  render() {
    const users = this.props.users;

    return (
      <div style={{ margin: '0 auto', padding: '20px', maxWidth: '600px' }}>
        <Alert
          color="danger"
          isOpen={!!this.props.users.error}
          toggle={this.handleCloseAlert}
        >
          {this.props.users.error}
        </Alert>
        <CreateUserForm />

        <UsersList
          users={users}
          deleteUserRequest={this.props.deleteUserRequest}
        />
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    users: state.users,
  };
};
export default connect(mapStateToProps, {
  getUsersRequest,
  deleteUserRequest,
  usersError,
})(App);
