import React from 'react';
import { Form, FormGroup, Input, Label, Button } from 'reactstrap';
import { connect } from 'react-redux';
import { createUserRequest } from '../actions/users';
class CreateUserForm extends React.Component {
  state = {
    firstName: '',
    lastName: '',
  };

  render() {
    return (
      <Form
        onSubmit={(e) => {
          e.preventDefault();
          this.props.createUserRequest(
            this.state.firstName,
            this.state.lastName
          );
          this.setState({ firstName: '', lastName: '' });
        }}
      >
        <FormGroup>
          <Label>FirstName:</Label>
          <Input
            name="firstName"
            value={this.state.firstName}
            onChange={(e) => {
              this.setState({ [e.target.name]: e.target.value });
            }}
            placeholder="Enter First Name"
          ></Input>
        </FormGroup>
        <FormGroup>
          <Label>FirstName:</Label>
          <Input
            name="lastName"
            value={this.state.lastName}
            onChange={(e) => {
              this.setState({ [e.target.name]: e.target.value });
            }}
            placeholder="Enter Last Name"
          ></Input>
        </FormGroup>
        <Button className="mb-4" block outline type="submit" color="primary">
          Submit
        </Button>
      </Form>
    );
  }
}

export default connect(null, { createUserRequest })(CreateUserForm);
