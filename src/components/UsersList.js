import React from 'react';
import { ListGroup, ListGroupItem, Button } from 'reactstrap';
export default function UsersList(props) {
  return (
    <div>
      <ListGroup>
        {props.users.items.map((user) => {
          return (
            <ListGroupItem key={user.id}>
              <section style={{ display: 'flex' }}>
                <div style={{ flexGrow: 1, margin: 'auto' }}>
                  {user.firstName} {user.lastName}
                </div>
                <div>
                  <Button
                    outline
                    color="danger"
                    onClick={() => {
                      props.deleteUserRequest(user.id);
                    }}
                  >
                    Delete
                  </Button>
                </div>
              </section>
            </ListGroupItem>
          );
        })}
      </ListGroup>
    </div>
  );
}
