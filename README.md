## User - Display List, Create New User, Delete User

## Table of contents

- [Preface](#Preface)
- [How to start](#How-to-start)
- [Technologies used for this project](#Technologies-used-for-this-project)
- [Contributors](#Contributors)

## Preface

This is a sample project to understand Redux Saga and explores following saga effects:

- takeEvery
- takeLatest
- take
- call
- put

## How to start

1. Clone the git repository into a new directory on your computer: `https://LavanyaJay@bitbucket.org/LavanyaJay/redux-saga-tutorial.git`
2. Run `npm install` on your terminal to install all the dependendencies
3. Run `npm start` to get a preview of the front-end

## Technologies used for this project

1.  React with `npm create-react-app my-app`
2.  `redux` and `react-redux` and `redux-saga` to set up a redux store and dispatch actions
3.  `axios` to fetch data from the database

## Contributors

- Lavanya Jayapalan | [Bit Bucket](https://bitbucket.org/LavanyaJay)
